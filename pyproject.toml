[tool.poetry]
name = "itemizr"
version = "0.0.1"
description = "itemizr app"
authors = ["Shawn McElroy <shawn@skift.io>"]
packages = [
    { include = "itemizr", from = "src"}
]

[tool.poetry.urls]
"Bug Tracker" = "https://gitlab.com/autoferrit/itemizr/issues"

[tool.poetry.dependencies]
python = "^3.8"
asyncpg = "^0.21.0"
ciso8601 = "^2.1.3"
click = "^7.1.2"
fastapi = "^0.61.1"
loguru = "^0.5.3"
pydantic = {extras = ["dotenv", "email"], version = "^1.6.1"}
python-slugify = "^4.0.1"
pytz = "^2020.1"
sentry-sdk = "^0.17.7"
tortoise-orm = "^0.16.15"
uvicorn = "^0.11.8"
wheel = "^0.35.1"
typing-extensions = "^3.7.4"


[tool.poetry.dev-dependencies]
autoflake = "^1.3.1"
autopep8 = "^1.5.3"
bandit = "^1.6.2"
black = "^20.8b1"
coverage = "^5.2.1"
dlint = "^0.10.3"
doc8 = "^0.8.0"
flake8 = "^3.8.3"
flake8-blind-except = "^0.1.1"
flake8-bugbear = "^20.1.4"
flake8-builtins = "^1.5.3"
flake8-comprehensions = "^3.2.2"
flake8-deprecated = "^1.3"
flake8-docstrings = "^1.5.0"
flake8-fixme = "^1.1.1"
flake8-isort = "^4.0.0"
flake8-logging-format = "^0.6.0"
flake8-mutable = "^1.2.0"
flake8-pytest = "^1.3"
flake8-variables-names = "^0.0.3"
gunicorn = "^20.0.4"
ipdb = "^0.13.3"
isort = "^5.2.2"
mypy = "^0.782"
pep8-naming = "^0.11.1"
pre-commit = "^2.6.0"
proselint = "^0.10.2"
ptpython = "^3.0.2"
pycodestyle = "^2.6.0"
pydocstyle = "^5.0.2"
pytest = "^6.0.1"
pytest-bandit = "^0.5.2"
pytest-cov = "^2.8.1"
pytest-mypy-plugins = "^1.4.0"
pytest-sugar = "^0.9.4"
recommonmark = "^0.6.0"
safety = "^1.8.7"
shellcheck-py = "^0.7.1"
sphinx = "^3.0.3"
supervisor = "^4.2.0"
timing-asgi = "^0.2.0"
yamllint = "^1.23.0"
typing_extensions = "^3.7.4"

[tool.poetry.scripts]
"itemizr" = "scripts:start"

[tool.black]
line-length = 88
target-version = ['py38']
include = '''
(
      \.pyi?$
    | \.py?$
    | ^/tests/
    | ^/src/itemizr
    | ^/src/config
)
'''
exclude = '''
(
  /(
      \.eggs         # exclude a few common directories in the
    | \.git          # root of the project
    | \.hg
    | \.mypy_cache
    | \.tox
    | \.venv
    | _build
    | __pycache__
    | buck-out
    | build
    | dist
    | .*/migrations
    | \.github
    | ci
    | node_modules
    | static
    | staticfiles
  )/
  | \.html
  | \.js
  | \.css
  | \.scss
)
'''

[tool.isort]
import_heading_firstparty = "Library"
import_heading_future = "Futures"
import_heading_local = "Local"
import_heading_stdlib = "Standard Library"
import_heading_thirdparty = "Third Party"
include_trailing_comma = true
indent = 4
known_first_party = "itemizr"
known_third_party = ["factory", "mailchimp3", "pytest", "sentry_sdk", "fastapi"]
line_length = 88
lines_after_imports = 2
lines_between_types = 1
multi_line_output = 3
use_parentheses = true

[bandit]
targets = ["src"]
exclude = "/test"
tests = "B101,B102,B301"

[build-system]
requires = ["poetry>=0.12", "wheel==*"]
build-backend = "poetry.masonry.api"
