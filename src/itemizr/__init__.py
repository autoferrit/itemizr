"""Itemizr init docstring."""
# Standard Library
from typing import Tuple


__version__: Tuple[int, int, int] = (0, 1, 0)
