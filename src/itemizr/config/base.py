"""Config base."""
# Standard Library
import logging
import os

from ipaddress import IPv4Address
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union

# Third Party
from pydantic import BaseSettings, EmailStr, HttpUrl, SecretStr, validator

# Library
from itemizr import __version__


_PROJECT_DIR = Path(__file__).parents[3]
_SRC_DIR = _PROJECT_DIR / "src"
_ENVS_DIR = _PROJECT_DIR / "environments"
_TEMPLATES_DIR = Path(os.getenv("TEMPLATES_DIR", _SRC_DIR / "templates"))
_ENVS = set()
for _, dirs, _ in os.walk(_PROJECT_DIR / "environments"):  # type: ignore
    for name in dirs:
        if not name.startswith("_"):
            _ENVS.add(name.lower())
    break

_ENVIRONMENT: str = os.environ.get("ENVIRONMENT")  # type: ignore


class Config(BaseSettings):
    """Main Configurator class."""

    ####################################################################################
    # ENVIRONMENT
    ENVIRONMENT: str = "development"

    @validator("ENVIRONMENT", pre=True)
    def validate_environment(cls, v: str) -> str:  # noqa: B902
        """Make sure log level is valid."""
        if isinstance(v, str):
            if v not in _ENVS:
                raise ValueError(
                    f"Invalid Environment. got [{v}] . "
                    f"should be one of [{', '.join(_ENVS)}]"
                )
            return v.upper()
        raise ValueError(v)

    DEBUG: bool = True if ENVIRONMENT != "production" else False
    IS_TEST: bool = True if ENVIRONMENT == "testing" else False
    VERSION: Tuple[Union[int, str], ...] = __version__

    PROJECT_NAME: str = _PROJECT_DIR.name
    PROJECT_DIR: Path = _PROJECT_DIR
    APP_DIR: Path = _SRC_DIR / "itemizr"
    DOCS_DIR: Path = PROJECT_DIR / "docs"

    ####################################################################################
    # DOMAIN and ACCOUNTS
    SERVER_HOST: Union[HttpUrl, IPv4Address, str] = "127.0.0.1"
    SERVER_PORT: int = 5000
    USERS_OPEN_REGISTRATION: bool = False

    ####################################################################################
    # User
    FIRST_SUPERUSER: str = f"admin@{PROJECT_NAME.lower()}.com"
    FIRST_SUPERUSER_PASSWORD: str = "admin"

    ####################################################################################
    # EMAIL & SMTP
    SMTP_TLS: bool = True
    SMTP_PORT: Optional[int] = None
    SMTP_HOST: Optional[str] = None
    SMTP_USER: Optional[str] = None
    SMTP_PASSWORD: Optional[str] = None

    EMAIL_HOST: Union[HttpUrl, IPv4Address, str] = (
        "example.dev" if str(SERVER_HOST).split(".")[0].isnumeric() else SERVER_HOST
    )
    EMAIL_RESET_TOKEN_EXPIRE_HOURS: int = 48
    EMAIL_TEMPLATES_DIR: Path = _TEMPLATES_DIR / "email"
    EMAILS_FROM_EMAIL: EmailStr = EmailStr(f"noreply@{EMAIL_HOST}")
    EMAILS_ENABLED: bool = False

    @validator("EMAILS_ENABLED", pre=True)
    def get_emails_enabled(cls, v: bool, values: Dict[str, Any]) -> bool:
        """Validate Emails."""
        bool_val: bool = True if v == "true" else False  # type: ignore
        return bool(
            bool_val
            and values.get("SMTP_HOST")
            and values.get("SMTP_PORT")
            and values.get("EMAILS_FROM_EMAIL")
        )

    EMAIL_TEST_USER: str = f"test_{PROJECT_NAME}@test.{EMAIL_HOST}"
    EMAILS_FROM_NAME: str = PROJECT_NAME

    ####################################################################################
    # API
    API_PREFIX: str = "/api"
    API_V0: str = "Alpha"
    API_V1: str = "1"
    API_URL: str = f"{API_PREFIX}/v{API_V0}"
    # 60 minutes * 24 hours * 8 days = 8 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8

    ####################################################################################
    # SECURITY
    SECRET_KEY: SecretStr
    FORCE_SSL: bool = not DEBUG and "production" == ENVIRONMENT
    #     Field(
    #     secrets.token_urlsafe(32),
    #     title="The Secret Key",
    #     description="provide a secret key",
    #     # min_length=32,
    #     exclusive_minimum=32,
    # )
    # BACKEND_CORS_ORIGINS is a JSON-formatted list of origins
    # e.g: '["http://localhost", "http://localhost:4200", "http://localhost:3000", \
    # "http://localhost:8080", "http://local.dockertoolbox.tiangolo.com"]'
    BACKEND_CORS_ORIGINS: List[HttpUrl] = []

    @classmethod
    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        """Validate the BACKAND_CORS_ORIGINS."""
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    ALLOWED_HOSTS: List[str] = ["example.com", "*.example.com"]
    if DEBUG or IS_TEST:
        ALLOWED_HOSTS += ["*"]

    @classmethod
    @validator("ALLOWED_HOSTS", pre=True)
    def assemble_allowed_hosts(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        """Validate the ALLOWED_HOSTS."""
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    ####################################################################################
    # LOGGING
    LOG_LEVEL: str = logging.getLevelName(logging.INFO)

    @validator("LOG_LEVEL", pre=True)
    def validate_log_level(cls, v: Union[str, int]) -> str:  # noqa: B902
        """Make sure log level is valid."""
        if isinstance(v, str):
            return v.upper()
        elif isinstance(v, int):
            return logging.getLevelName(v)
        raise ValueError(v)

    JSON_LOGS: bool = True if os.environ.get("JSON_LOGS", "0") == "1" else False

    @validator("JSON_LOGS", pre=True)
    def validate_json_logs(cls, v: Union[str, int]) -> bool:
        """Validate json logs."""
        if isinstance(v, int):
            return False if v == 0 else True
        elif isinstance(v, str):
            return True if v.lower() == "true" else False
        raise ValueError(
            "Invalid value for JSON_LOGS. should be one of 'true' or " "'false'"
        )

    ####################################################################################
    # DATABASE
    USE_SQLITE: bool = False
    DB_HOST: Optional[str] = "127.0.0.1"
    DB_PORT: Optional[int] = 5432
    DB_USER: Optional[str] = PROJECT_DIR.name
    DB_PASS: Optional[str] = PROJECT_DIR.name
    DB_NAME: Optional[str] = PROJECT_DIR.name

    GENERATE_SCHEMAS: bool = True
    DB_MODELS: Optional[str] = "itemizr.apps.models"

    @validator("DB_MODELS", always=True)
    def check_db_models(cls, v: str) -> str:  # noqa: B902
        """Validate the DB_MODELS."""
        if isinstance(v, str):
            return ",".join([i.strip() for i in v.split(",")])
        raise ValueError(v)

    ####################################################################################
    # TEMPLATING
    TEMPLATES_DIR: Path = _TEMPLATES_DIR

    ####################################################################################
    # MISC
    SENTRY_DSN: Optional[HttpUrl] = None

    def get_version(self) -> str:
        """Get current app version."""
        return ".".join(str(part) for part in self.VERSION)
