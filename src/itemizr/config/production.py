"""Production config file."""
# Standard Library
from typing import Optional

# Third Party
from pydantic import HttpUrl, validator

# Library
from itemizr.config.base import Config


class ProdConfig(Config):
    """Production Configurator."""

    SENTRY_DSN: Optional[HttpUrl] = None

    @validator("SENTRY_DSN")
    def sentry_dsn_can_be_blank(cls, v: str) -> Optional[str]:
        """Validate SENTRY_DSN."""
        if len(v) == 0:
            return None
        return v
