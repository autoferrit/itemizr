"""Config helper."""
# Standard Library
import os
import sys

from functools import lru_cache
from pathlib import Path
from typing import Dict, Union

# Third Party
from loguru import logger

from .base import _ENVIRONMENT, _ENVS, _ENVS_DIR, Config
from .production import ProdConfig


ConfigType = Union[Config, ProdConfig]


@lru_cache()
def get_config() -> ConfigType:
    """Get the current config."""
    environment: str = os.getenv("ENVIRONMENT", "development")
    if environment not in _ENVS:
        logger.error(
            f"Invalid Environment. got: [{str(environment)}]\n"
            f"Available ENVS: [{', '.join(_ENVS)}]"
        )
        raise sys.exit(1)

    options: Dict[str, Union[Path, str]] = {
        "ENVIRONMENT": environment if environment else _ENVIRONMENT,
    }
    options["_env_file"] = _ENVS_DIR / str(options["ENVIRONMENT"]) / ".env"

    cfg = Config if "production" != options["ENVIRONMENT"] else ProdConfig
    config = cfg(**options)  # type: ignore # noqa

    logger.info("CONFIG: {}", cfg)
    logger.info("APP: {}", config.PROJECT_NAME)
    logger.info("VERSION: {}", config.get_version())
    logger.info("ENVIRONMENT: {}", config.ENVIRONMENT)
    logger.info("ENV_FILE: {}", options["_env_file"])
    logger.info("DEBUG: {}", config.DEBUG)
    logger.info("IS_TEST: {}", config.IS_TEST)

    return config


__all__ = ["get_config", "Config", "ProdConfig", "ConfigType"]
