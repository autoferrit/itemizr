"""itemizr logging."""
# Futures
from __future__ import annotations

# Standard Library
import logging
import sys

from pprint import pformat
from typing import Dict, Union

# Third Party
import loguru

from loguru import logger as loguru_logger
from loguru._defaults import LOGURU_FORMAT  # noqa


class InterceptHandler(logging.Handler):
    """Logging intercept handler."""

    def emit(self, record: logging.LogRecord) -> None:  # pragma: no cover
        """Log the message."""
        try:
            level: Union[int, str] = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno

        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back  # type: ignore
            depth += 1

        log = logger.bind(request_id="app")
        log.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


def _format_record(record: Dict[str, Dict[str, str]]) -> str:
    """
    Custom format for loguru loggers.
    Uses pformat for log any data like request/response body during debug.
    Works with logging if loguru handler it.
    """
    format_string = LOGURU_FORMAT

    if record["extra"].get("payload") is not None:
        record["extra"]["payload"] = pformat(
            record["extra"]["payload"], indent=4, compact=True, width=88
        )
        format_string += "\n<level>{extra[payload]}</level>"

    format_string += "{exception}\n"
    return format_string


def _get_logger() -> loguru.Logger:
    """Setup the logger."""
    # set format
    loguru_logger.configure(
        handlers=[
            {"sink": sys.stdout, "level": logging.DEBUG, "format": _format_record}
        ]
    )

    # works with uvicorn>=0.11.6
    loggers = (
        logging.getLogger(name)
        for name in logging.root.manager.loggerDict  # type: ignore # noqa
        if name.startswith("uvicorn.")
    )
    for uvicorn_logger in loggers:
        uvicorn_logger.handlers = []

    logging.getLogger("uvicorn").handlers = [InterceptHandler()]

    return loguru_logger


logger = _get_logger()
