# Third Party
from fastapi.applications import FastAPI

# Library
from itemizr.config import Config, get_config
from itemizr.logging import logger


def get_application(cfg: "Config") -> FastAPI:
    """Main app factory."""
    application = FastAPI(
        title=cfg.PROJECT_NAME,
        version=".".join(str(v) for v in cfg.VERSION),
        description="Savage Worlds Items Generator",
        debug=cfg.DEBUG,
        openapi_url=f"{cfg.API_URL}/openapi.json",
        logger=logger,
    )

    init(application)

    return application


def init(application: FastAPI) -> None:
    """Initialize the app."""


config = get_config()

app = get_application(config)
