# All configuration for plugins and other utils is defined here.
# Read more about `setup.cfg`:
# https://docs.python.org/3/distutils/configfile.html


[flake8]
show-source = True
statistics = False
max-line-length = 88
exclude = .tox,.git,*/migrations/*,*/static/CACHE/*,docs,node_modules,__pycache__,.venv,venv,.eggs,*.egg
doctests = True
enable-extensions = G

# Flake plugins:
max-complexity = 8
accept-encodings = utf-8
radon-max-cc = 10
radon-show-closures = True
radon-no-assert = True

# Excluding some directories:
select = BLK,B,C,DUO,E,F,W,T4,T100,B9

# Disable some pydocstyle checks:
# C819,   # flake8-commas - trailing comma prohibited
# D100,   # Missing docstring in public module
# D104,   # Missing docstring in public package
# D401,   # pydocstyle - First line should be in imperative mood
# DAR103, # darglint - The docstring parameter type doesn't match function.
# DAR203, # darglint - The docstring parameter type doesn't match function.
# E203,   # pycodestyle - whitespace before ‘:’
#           gives false positive for slices. remove when this is fixed?
#           https://github.com/PyCQA/pycodestyle/issues/373
# E266,   # pycodestyle - too many leading ‘#’ for block comment
# E501,   # pycodestyle - line too long (82 > 79 characters)
# F401,   # module imported but unused
# F403,   # ‘from module import *’ used; unable to detect undefined names
# Q000,   # flake8-quotes - Remove bad quotes
# RST303,
# RST304,
# W503,   # pycodestyle - line break before binary operator
# W504,   # pycodestyle - line break after binary operator
# WPS305,
# X100
extend-ignore = BLK100, B950, C819, D100, D104, D401, DAR103, DAR203,
    E203, E231, E266, E501, F401, F403, G004, Q000, RST303, RST304,
    W503, W504, WPS305, X100

# Docs: https://github.com/snoack/flake8-per-file-ignores
# You can completely or partially disable our custom checks,
# to do so you have to ignore `WPS` letters for all python files:
per-file-ignores =
  # Allow `__init__.py` with logic for configuration:
  src/config/**/*.py: WPS226, WPS407, WPS412, WPS432
  # Allow to have magic numbers inside migrations and wrong module names:
  src/bohemian/*/migrations/*.py: WPS102, WPS432
  # Enable `assert` keyword and magic numbers for tests:
  tests/*.py: S101, WPS432

[pycodestyle]
max-line-length = 88
exclude = .tox,.git,*/migrations/*,*/static/CACHE/*,docs,node_modules

[pydocstyle]
ignore = D100,D104,D203,D213
match = .*\.py

[isort]
import_heading_future=Futures
import_heading_stdlib=Standard Library
import_heading_thirdparty=Third Party
import_heading_firstparty=Library
import_heading_local=Local
include_trailing_comma = true
indent=4
line_length = 120
multi_line_output=3


[mypy]
# Mypy configuration:
# https://mypy.readthedocs.io/en/latest/config_file.html
python_version = 3.8
allow_redefinition = False
check_untyped_defs = True
disallow_any_explicit = False
disallow_any_generics = True
disallow_untyped_calls = True
disallow_untyped_decorators = False
disallow_untyped_defs = True
ignore_errors = False
ignore_missing_imports = True
implicit_reexport = True
no_implicit_optional = True
strict_equality = True
strict_optional = True
warn_no_return = True
warn_redundant_casts = True
warn_unreachable = True
warn_unused_configs = True
warn_unused_ignores = True

plugins = pydantic.mypy

[pydantic-mypy]
init_forbid_extra = True
init_typed = True
warn_required_dynamic_aliases = True
warn_untyped_fields = True

[mypy-*.migrations.*]
# Django migrations should not produce any errors:
ignore_errors = True

[mypy-*.tests.*]
allow_untyped_decorators = True

[coverage:paths]
source =
  src/bohemian

[tool.coverage:run]
branch = False
command_line = "-m pytest"
include = src/bohemian/*
omit = *migrations*, *tests*
plugins =
    django_coverage_plugin

[darglint]
# darglint configuration:
# https://github.com/terrencepreilly/darglint
strictness = long

[doc8]
ignore-path = docs/_build
max-line-length = 88
sphinx = True

[bandit]
targets = ["src"]
exclude = "/test"
;tests = "B101,B102,B301"
